#![no_std]
#![no_main]

use arrayvec::ArrayString;
use embedded_graphics::fonts::{Font6x8, Text};
use embedded_graphics::pixelcolor::Rgb565;
use embedded_graphics::prelude::*;
use embedded_graphics::primitives::Rectangle;
use embedded_graphics::{primitive_style, text_style};
use longan_nano::hal::{pac, prelude::*};
use longan_nano::{hal, lcd, lcd_pins};
use riscv_rt::entry;


use core::fmt::Write;


use core::panic::PanicInfo;
use core::sync::atomic::{self, Ordering};

/// This function is called on panic.
#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {
        atomic::compiler_fence(Ordering::SeqCst);
    }
}


#[entry]
fn main() -> ! {
    let dp = pac::Peripherals::take().unwrap();

    // Configure clocks
    let mut rcu = dp
        .RCU
        .configure()
        .ext_hf_clock(8.mhz())
        .sysclk(108.mhz())
        .freeze();
    let mut afio = dp.AFIO.constrain(&mut rcu);

    let gpioa = dp.GPIOA.split(&mut rcu);
    let gpiob = dp.GPIOB.split(&mut rcu);

    let lcd_pins = lcd_pins!(gpioa, gpiob);
    let mut lcd = lcd::configure(dp.SPI0, lcd_pins, &mut afio, &mut rcu);
    let (width, height) = (lcd.size().width as i32, lcd.size().height as i32);

    // Clear screen
    Rectangle::new(Point::new(0, 0), Point::new(width - 1, height - 1))
        .into_styled(primitive_style!(fill_color = Rgb565::BLACK))
        .draw(&mut lcd)
        .unwrap();

    let style = text_style!(
        font = Font6x8,
        text_color = Rgb565::BLACK,
        background_color = Rgb565::GREEN
    );

    Text::new("flash:", Point::new(10, 05))
        .into_styled(style)
        .draw(&mut lcd)
        .unwrap();
    
    let mut num_buf = ArrayString::<3>::new();;
    write!(&mut num_buf, "{}", hal::signature::flash_size_kb()).unwrap();
    Text::new(&num_buf, Point::new(40, 05))
        .into_styled(style)
        .draw(&mut lcd)
        .unwrap();

    Text::new("ram:", Point::new(10, 15))
        .into_styled(style)
        .draw(&mut lcd)
        .unwrap();
    
    let mut num_buf = ArrayString::<3>::new();;
    write!(&mut num_buf, "{}", hal::signature::sram_size_kb()).unwrap();
    Text::new(&num_buf, Point::new(40, 15))
        .into_styled(style)
        .draw(&mut lcd)
        .unwrap();

    {
        Text::new("id:", Point::new(10, 25))
            .into_styled(style)
            .draw(&mut lcd)
            .unwrap();

        let mut id_bufs = [ArrayString::<8>::new(); 3];
        for (i, byte) in hal::signature::device_id().iter().enumerate() {
            write!(&mut id_bufs[i], "{:08x}", byte).unwrap();
        }

        for (i, buf) in id_bufs.iter().enumerate() {
            Text::new(&buf, Point::new(40, 25 + (i as i32 * 10)))
                .into_styled(style)
                .draw(&mut lcd)
                .unwrap();
        }
    }

    loop {}
}
