#![no_std]
#![no_main]

use arrayvec::ArrayString;
use embedded_graphics::fonts::{Font6x8, Text};
use embedded_graphics::pixelcolor::Rgb565;
use embedded_graphics::prelude::*;
use embedded_graphics::primitives::Rectangle;
use embedded_graphics::{primitive_style, text_style};
use longan_nano::hal::{pac, prelude::*};
use longan_nano::{hal, lcd, lcd_pins};
use riscv_rt::entry;
use riscv_cross;


use core::fmt::Write;


use core::panic::PanicInfo;
use core::sync::atomic::{self, Ordering};

/// This function is called on panic.
#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {
        atomic::compiler_fence(Ordering::SeqCst);
    }
}

#[used]
#[no_mangle]
pub static mut START_TIMESTAMP: u32 = 5;

#[entry]
fn main() -> ! {
    let mut dp = pac::Peripherals::take().unwrap();

    // Configure clocks
    let mut rcu = dp
        .RCU
        .configure()
        .ext_hf_clock(8.mhz())
        .sysclk(108.mhz())
        .freeze();
    let mut afio = dp.AFIO.constrain(&mut rcu);

    let gpioa = dp.GPIOA.split(&mut rcu);
    let gpiob = dp.GPIOB.split(&mut rcu);

    let lcd_pins = lcd_pins!(gpioa, gpiob);
    let mut lcd = lcd::configure(dp.SPI0, lcd_pins, &mut afio, &mut rcu);
    let (width, height) = (lcd.size().width as i32, lcd.size().height as i32);

    // Clear screen
    Rectangle::new(Point::new(0, 0), Point::new(width - 1, height - 1))
        .into_styled(primitive_style!(fill_color = Rgb565::BLACK))
        .draw(&mut lcd)
        .unwrap();

    let style = text_style!(
        font = Font6x8,
        text_color = Rgb565::BLACK,
        background_color = Rgb565::GREEN
    );

    let mut bkp = dp.BKP.configure(&mut rcu, &mut dp.PMU);
    let mut rtc = hal::rtc::Rtc::rtc(dp.RTC, &mut bkp);

    // check if backup domain was reset    
    let bkp_regs = unsafe { &*pac::BKP::ptr() };
    let reset_flag = bkp_regs.data0.read().bits();
    if reset_flag == 0 {
        // init RTC
        rtc.select_frequency(1.hz());
        rtc.set_time(unsafe { START_TIMESTAMP });
        
        bkp_regs.data0.write(|w| unsafe { w.bits(0b1) });
    }
    let rtc = rtc;

    Text::new("reset:", Point::new(10, 05))
        .into_styled(style)
        .draw(&mut lcd)
        .unwrap();
    
    let mut num_buf = ArrayString::<3>::new();;
    write!(&mut num_buf, "{}", reset_flag).unwrap();
    Text::new(&num_buf, Point::new(40, 05))
        .into_styled(style)
        .draw(&mut lcd)
        .unwrap();


    loop {
        Text::new("TIME:", Point::new(10, 55))
            .into_styled(style)
            .draw(&mut lcd)
            .unwrap();
        
        let mut num_buf = ArrayString::<12>::new();;
        write!(&mut num_buf, "{}", rtc.current_time()).unwrap();
        Text::new(&num_buf, Point::new(50, 55))
            .into_styled(style)
            .draw(&mut lcd)
            .unwrap();
    }
}
